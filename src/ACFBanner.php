<?php
namespace Dudley\Patterns\Pattern\Banner;

/**
 * Class ACFBanner
 *
 * @package Dudley\Patterns\Pattern\Banner
 */
class ACFBanner extends Banner {
	/**
	 * ACF meta type
	 *
	 * @var string
	 */
	public static $meta_type = 'acf';

	/**
	 * ACFBanner constructor.
	 */
	public function __construct() {
		if ( ! get_field( 'banner_show' ) ) {
			return;
		}

		parent::__construct( get_field( 'banner_image' ), get_field( 'banner_heading' ) );
	}
}
