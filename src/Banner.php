<?php
namespace Dudley\Patterns\Pattern\Banner;

use Dudley\Patterns\Abstracts\AbstractBanner;
use Dudley\Patterns\Traits\HeadingTrait;

/**
 * Class Banner
 *
 * @package Dudley\Patterns\Pattern\Banner
 */
abstract class Banner extends AbstractBanner {
	use HeadingTrait;

	/**
	 * @var string
	 */
	public static $action_name = 'banner';

	/**
	 * Banner constructor.
	 *
	 * @param array  $image
	 * @param string $heading
	 */
	public function __construct( $image, $heading ) {
		parent::__construct( $image, 'large' );
		$this->heading = $heading;
	}

	/**
	 * @return array
	 */
	public function requirements() {
		return array_merge( parent::requirements(), [
			$this->heading,
		]);
	}
}
