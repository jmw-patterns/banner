<?php
/**
 * @var $module Dudley\Patterns\Pattern\Banner\Banner
 */
?>

<section class="banner" style="background-image: url(<?php $module->img_url(); ?>);">
	<div class="banner__inner">
		<h2 class="banner__hd"><?php $module->heading(); ?></h2>
	</div>
</section><!-- .banner -->
